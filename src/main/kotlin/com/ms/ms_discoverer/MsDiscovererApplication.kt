package com.ms.ms_discoverer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableEurekaServer
class MsDiscovererApplication

fun main(args: Array<String>) {
	runApplication<MsDiscovererApplication>(*args)
}
